# calendario-melhorias

## Instalação
```shell
docker-compose up
```

## Carregando dados na base
```shell
docker exec -it calendariomelhorias_database_1 bash
cd /home
psql -U postgres -f database.sql
psql -U postgres -f seeds/seeds.sql
```

## Instalando dependências
```shell
docker exec -it calendariomelhorias_web_1 bash
chmod +x composer
php composer install
```

## Portas
* :8056

#### Correções de bugs e melhorias
~~*Você deve criar um merge request com sua correção.*~~
Crie um fork do projeto com as suas correções
