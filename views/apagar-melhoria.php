<?php

/*
Página responsável por:
Mostrar confirmação da exclusão de melhoria;
Excluir melhoria.
*/

use DAO\Melhoria;

// Exclusão após receber id por post
if(!empty($_POST['id'])) {
  $resposta = Melhoria::getInstance()->delete($_POST['id']);
  require_once ('views/agenda.php');
  die();
}
// Antes da exclusão confirmar nome da área
else if (!empty($_GET['id'])) {
  $melhoria = Melhoria::getInstance()->filtrarPorId($_GET['id']);
  $id = $melhoria->id;
  $descricao = $melhoria->descricao;
} else {
  require_once ('views/agenda.php');
  die();
}
?>

<?php if(isset($_GET['id'])) : ?>

  <div class="container">
    <form action="/?path=apagar-melhoria" method="POST">

      <div class="alert alert-danger" role="alert">
        <p>A melhoria/tarefa abaixo será apagada, tem certeza disso?</p>
        <blockquote class="blockquote">
          <p class="mb-0">"<?=$descricao?>"</p>
        </blockquote>
      </div>
      <input type="hidden" name="id" value="<?=$id?>">
      <button type="submit" class="btn btn-primary">Sim</button>
    </form>
  </div>

<?php endif; ?>
