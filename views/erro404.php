<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">404</h1>
    <p class="lead">A página que está procurando não existe...</p>
    <hr class="my-4">
    <p>Por favor, volte para o ínicio do site.</p>
    <a class="btn btn-primary btn-lg" href="/index.php" role="button">Voltar</a>
  </div>
</div>
