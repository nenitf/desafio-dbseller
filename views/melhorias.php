<?php

/*
Página responsável por:
Listagem de melhorias.
*/

use DAO\Melhoria;
use DAO\Area;

$melhorias = Melhoria::getInstance()->order('id', 'desc')->getAll();
?>
<div class="container">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Área</th>
        <th scope="col">Apagar</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($melhorias as $melhoria) : ?>
        <tr>
          <th scope="row"><?=$melhoria->id?></th>
          <td><?=mb_substr($melhoria->descricao, 0, 70, 'UTF-8')?>...</td>
          <td><?=Area::getInstance()->filtrarPorId($melhoria->area)->descricao;?></td>
          <td><a class="btn btn-danger" role="button" href="?path=apagar-melhoria&id=<?=$melhoria->id?>"><i class="fas fa-trash"></i></a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
 
